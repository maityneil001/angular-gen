/**
 * @ngdoc service
 * @name app.Route
 * @description
 * # ROUTE
 * Constant in the wcApp.
 */
(function() {
    'use strict';
    angular.module('app').provider('ROUTE', ROUTE);

    function ROUTE() {
        return ({
            $get: function() {
                return {
                    pages: [],
                    otherwise:{
                        templateUrl: 'modules/welcome/welcome.html',
                        controller: 'WelcomeController',
                    }
                };
            }
        });
    }
})();

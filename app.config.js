(function(){
  'use strict';
  angular.module('app').config(config);

  function config($routeProvider,ROUTEProvider){
      /**
       * Loop all routes in ROUTE provider
       */
      angular.forEach(ROUTEProvider.$get().pages, function(page) {
          $routeProvider.when(page.url, page.routingParams);
      });
      /**
       * Error fallback
       */
      $routeProvider.otherwise(ROUTEProvider.$get().otherwise);
  }
})();

(function(){
  'use strict';
  angular.module('app',[
    'ngRoute',
    'ui.router',
    'welcome',
    'ngSanitize',
    'ngStorage',
    'ngMaterial'  
  ]);
})();

module.exports = function(grunt) {
	// 1. All configuration goes here
	grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),
      // Mocha
      mocha: {
        all: {
          src: ['tests/testrunner.html'],
        },
        options: {
          run: true
        }
      },
      imagemin: {
        dynamic: {                         // Another target
          files: [{
            expand: true,                  // Enable dynamic expansion
            cwd: 'public/images/',                   // Src matches are relative to this path
            src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
            dest: 'release/images/'                  // Destination path prefix
          }]
        }
      },
      ngAnnotate: {
          options: {
              singleQuotes: true
          },
          app: {
              files: {
                  './public/scripts/min-safe/main.js': 
                    [
                        // BOWER COMPONENTS START  
                        'bower_components/angular/angular.js',
                        'bower_components/angular-route/angular-route.js',
                        'bower_components/angular-ui-router/release/angular-ui-router.js',
                        'bower_components/angular-animate/angular-animate.js',
                        'bower_components/angular-aria/angular-aria.js',
                        'bower_components/angular-messages/angular-messages.js',
                        'bower_components/angular-material/angular-material.js',
                        'bower_components/ngstorage/ngStorage.js',
                        'bower_components/angular-sanitize/angular-sanitize.js',
                        // BOWER COMPONENTS END
                        'app.module.js',
                        'config/*.js',
                        'app.config.js',
                        'modules/welcome/welcome.module.js',
                        'modules/welcome/welcome.controller.js'
                    ],
              }
          }
      },
    	concat: {
        	dist: {
            	src: [
                  './public/scripts/min-safe/main.js'
            	],
            	dest: 'release/script/main.js',
        	}
    	},
    	uglify: {
        	build: {
            	src: 'release/script/main.js',
            	dest: 'release/script/main.min.js'
        	}
    	},
      sass: {
        dist: {
          files: [{
            expand: true,
            cwd: 'public/style/sass',
            src: ['*.scss'],
            dest: 'public/style/css',
            ext: '.css'
          }]
        }
      },
      concat_css: {
        options: {
          // Task-specific options go here.
        },
        all: {
          src: ["bower_components/angular-material/angular-material.css", "public/style/css/*.css"],
          dest: "release/css/main.css"
        },
      },
      cssmin: {
        target: {
          options:{
            sourceMap:true,
          }, 
          files: [{
            expand: true,
            cwd: 'release/css',
            src: ['main.css'],
            dest: 'release/css',
            ext: '.min.css'
          }]
        }
      },
      connect: {
        server: {
          options: {
            port: 3000,
            base: './',
            livereload: true,
            open:true,
          }
        }
      },
    	watch: {
        	options: {
            	livereload: true,
        	},
        	scripts: {
            	files: ['app.module.js','app.config.js','config/*.js','plugins/**/*.js','modules/**/*.js'],
            	tasks: ['ngAnnotate','concat', 'uglify'],
            	options: {
                  atBegin: true,
                	spawn: false,
                  interrupt: true,
            	},
        	},
        	css: {
            	files: ['public/style/sass/*.scss'],
            	tasks: ['sass','concat_css'],
            	options: {
                  atBegin: true,
                	spawn: false,
                  interrupt: true,
            	}
        	}
    	}
	});
	// 3. Where we tell Grunt we plan to use this plug-in.
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-mocha');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-connect'); 
	// 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
	grunt.registerTask('default', ['connect:server','watch']);
  // grunt.registerTask('default', ['ngAnnotate']);
  grunt.registerTask('test', ['mocha']);
};
